package com.helloworld.alex0.helloworld

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import java.util.*

import com.helloworld.alex0.helloworld.common.nameCommon


// FONCTIONS :
fun isOldEnough(age:Int): Boolean {
    return age >=5
}

fun describePeople(name:String, age:Int, height:Float){
    val canPlayString = when(isOldEnough(age)){
        true -> "peut jouer au basket"
        false -> "ne peut pas jouer au basket"
    }
    println("${name} a ${age} ans, mesure ${height}m et ${canPlayString}")
}

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //DECLARATION VARIABLE var / val
        var name = "Bob"
        var age = 10

        println("String template complexe : ${name.toUpperCase()} a $age ans")

        println("Concatenation : " + "Nom : " + name + "Age : " + age)

        println(
            """
            |Nom : $name
            |Age : $age
        """.trimMargin()
        )

        //CONDITION IF WHEN
        if (age > 5 && age < 10) {
            println("age ok")
        } else {
            println("rip")
        }

        when (age) {
            5 -> println("prout")
            10 -> println("lol")
            else -> println("one ne connait pas")
        }

        when (age) {
            in 1..5 -> println("${name} est trop jeune")
            in 6..10 -> println("${name} peut jouer au basket")
            !in 1..18 -> println("trop vieux")
            else -> println("balec")
        }

        val canPlay = when (age) {
            in 5..10 -> true
            else -> false
        }

        // MUTABLE
        var name2: String? = "Bob"

        println(name2?.length)
        var canPlayBasket: Boolean = false
        canPlayBasket = isOldEnough(10)
        var height: Float = 1.75F
        describePeople(name,age, height)

        name = "Bobette"
        age = 4
        height = 1.8F
        describePeople(name,age, height)

        // TABLEAU
        val ages = Array<Int>(10) {7}
        ages.set(0, 5)
        println(Arrays.toString(ages))
        val tableau = arrayOf(1,2,3,4)

        ages.get(0)

        //BOUCLE FOR
        for (i in 1..5){
            println(i)
        }
        for (i in 1..5 step 2){
            println(i)
        }

        for (i in 5 downTo 1){
            println(i)
        }

        val names: Array<String> = arrayOf("Bob", "Bobette", "Toto", "Alexis")

        for(i in 0..names.size - 1){
            println(names[i])
        }
        //avec For each
        for(name in names){
            println(name)
        }
        //Savoir l'index avec for each
        for((index,name) in names.withIndex()){
            println("$name est à l'index $index")
        }

        for(name in names){
            if(name == "Toto"){
                println("$name est absent(e)")
                continue
            }
            println("$name est présent(e)")
        }

        // Boucle While

        var email = 3

        while(email > 0){
            println("vous avez $email emails non lus")
            email--
        }

        //Package
        println(nameCommon)

    }


}
